########################################
### CHAPITRE 2 --> Installer Node.js ###
########################################

_Tester Node.js avec un programme minimal_

Il est temps de vérifier que Node.js fonctionne bien ! Nous allons écrire un tout tout petit programme pour commencer, qui se contente d'afficher un message dans la console. Ce sera l'occasion de voir comment fonctionne l'exécution de fichiers .js avec Node.

###########################################################
### CHAPITRE 3 --> Une première application avec NodeJS ###
###########################################################

C'est un des chapitres les plus importants du cours car il introduit de nombreux concepts de Node.js, qui seront pour la plupart nouveaux pour vous. Il faudra donc lire ce chapitre dans un environnement calme, progressivement, et ne pas hésiter à le relire une seconde fois le lendemain pour vous assurer que vous avez bien compris.

    Dans ce chapitre, nous allons créer une vraie application Node.js de bout en bout.
Vous allez voir ce que bas niveau veut dire ! Nous allons en effet devoir gérer tous les rouages du serveur web, qui va traiter les requêtes HTTP des visiteurs et leur retourner une page web HTML.

Ce sera pour vous l'occasion d'expérimenter les fameux callbacks dont je vous avais parlé dans le premier chapitre, ces fonctions qui s'exécutent dès lors qu'un évènement survient. Node.js en est rempli, vous ne pourrez pas y échapper ! 

###########################################################
### CHAPITRE 4                                          ###
###########################################################
_QUIZ 1_

###########################################################
### CHAPITRE 5 --> Les évènements ###
###########################################################
Node.js est un environnement de développement JavaScript basé sur les évènements. Je vous ai montré dans le premier chapitre ce que ça signifie : il y a un seul thread mais aucune opération n'est bloquante. Ainsi, les opérations un peu longues (chargement d'un fichier, téléchargement d'une page web, démarrage d'un serveur web...) sont lancées en tâche de fond et une fonction de callback est appelée quand l'opération est terminée.


###########################################################
### CHAPITRE 6 --> Les modules Node.js et NPM ###
###########################################################
Croyez-le ou non, le noyau de Node.js est tout petit. De base, Node.js ne sait en fait pas faire grand chose.
Pourtant, Node.js est très riche grâce à son extensibilité. Ces extensions de Node.js sont appelées modules.

Il existe des milliers de modules qui offrent des fonctionnalités variées : de la gestion des fichiers uploadés à la connexion aux bases de données MySQL ou à Redis, en passant par des frameworks, des systèmes de templates et la gestion de la communication temps réel avec le visiteur ! Il y a à peu près tout ce dont on peut rêver et de nouveaux modules apparaissent chaque jour.

Nous allons commencer par voir comment sont gérés les modules par Node.js et nous verrons que nous pouvons facilement en créer un nous aussi. Puis, nous découvrirons NPM (Node Package Manager), l'outil indispensable qui vous permet de télécharger facilement tous les modules de la communauté Node.js ! Enfin, je vous montrerai comment accéder à la gloire éternelle en publiant votre module sur NPM. 

###########################################################
### CHAPITRE 7 --> Le framework Express.js              ###
###########################################################
    Bon, coder à bas niveau tous les détails c'est amusant, mais est-ce qu'il n'existe pas des moyens d'aller un peu plus vite par hasard ? Des frameworks par exemple ? 
Vous avez bien raison. Tout coder à la main ça va 5 minutes. Ca peut être utile dans certains cas précis d'ailleurs, mais dans la plupart des cas on aime avoir des outils à disposition pour aller plus vite. C'est pour cela qu'on a inventé les bibliothèques, puis les frameworks qui sont des sortes de super-bibliothèques.
Si vous flânez un peu sur NPM, vous allez vite voir qu'il existe un module plébiscité : Express.js. Il s'agit en fait d'un micro-framework pour Node.js. Il vous fournit des outils de base pour aller plus vite dans la création d'applications Node.js.

Mais attention : n'allez pas comparer Express.js avec des poids lourds comme Django ou Symfony2 ! Ceux-ci vous offrent des fonctionnalités très complètes et puissantes (comme la génération d'interfaces d'administration), ce qui n'est pas vraiment le cas d'Express.

Pourquoi ? 

Parce qu'on part de loin avec Node.js. Express vous permet donc d'être "un peu moins bas niveau" et de gérer par exemple plus facilement les routes (URL) de votre application et d'utiliser des templates. Rien que ça, ça va déjà être une petite révolution pour nous !
Pour suivre ce chapitre, créez-vous un dossier pour faire une application de test Node.js. Installez-y Express avec la commande :
npm install express

Vous voilà prêts à utiliser Express !

###########################################################
### CHAPITRE 8 --> TP : La ToDO List                    ###
###########################################################
Je crois qu'il est grand temps de pratiquer un peu ! Nous avons fait le tour d'un bon nombre de fonctionnalités de base de Node.js et nous avons même appris à nous servir du micro-framework Express.

Pourtant, on n'a vraiment le sentiment d'avoir compris que lorsqu'on a pratiqué et réalisé une première vraie application. C'est ce que je vous propose de faire dans ce TP. 

Nous allons réaliser ici une todo list (une liste de tâches). Le visiteur pourra simplement ajouter et supprimer des tâches.

Nous ferons donc quelque chose de très simple pour commencer, que vous pourrez ensuite améliorer comme bon vous semble !


###########################################################
### CHAPITRE 9                                          ###
###########################################################
_QUIZ 2_


###########################################################
### CHAPITRE 10 --> socket.io : passez au temps réel !  ###
###########################################################
socket.io est l'une des bibliothèques les plus prisées par ceux qui développent avec Node.js. Pourquoi ? Parce qu'elle permet de faire très simplement de la communication synchrone dans votre application, c'est-à-dire de la communication en temps réel !
Vous ne voyez pas ce que ça signifie ? Laissez-moi vous le dire autrement : socket.io vous permet par exemple de mettre en place très facilement un Chat sur votre site ! 
Les possibilités que vous offre socket.io sont en réalité immenses et vont bien au-delà du Chat : tout ce qui nécessite une communication immédiate entre les visiteurs de votre site peut en bénéficier. Ca peut être par exemple une brique de base pour mettre en place un jeu où on voit en direct les personnages évoluer dans le navigateur, le tout sans avoir à recharger la page !
Ca donne envie, avouez ! 

