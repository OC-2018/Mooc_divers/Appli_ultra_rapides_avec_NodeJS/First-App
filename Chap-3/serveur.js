const http = require('http');

const hostname = '127.0.0.1';
const port = 8080;


var server = http.createServer(function (req, res) {
    res.statusCode= 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello people, this is my first app with NodeJS');
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}`);
});
