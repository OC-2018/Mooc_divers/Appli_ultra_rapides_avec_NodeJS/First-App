var express = require('express');
var ent = require('ent');
var session = require('cookie-session'); // Charge le middleware de sessions
var bodyParser = require('body-parser'); // Charge le middleware de gestion des paramètres
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var fs = require('fs');

var app = express();


/* On utilise les sessions */
app.use(session({ secret: 'todotopsecret' }))


    /* Si aucune todolist existe , on encrée une */
    .use(function (req, res, next) {
        if (typeof (req.session.todolist) == 'undefined') {
            req.session.todolist = [];
        }
        next();
    })

    /* On affiche la todolist et le formulaire */
    .get('/todo', function (req, res) {
        res.render('todo.ejs', { todolist: req.session.todolist });
    })


    /* On ajoute un élément à la todolist */
    .post('/todo/ajouter/', urlencodedParser, function (req, res) {
        if (req.body.newtodo != '') {
            req.session.todolist.push(req.body.newtodo);
        }
        res.redirect('/todo');
    })

    /* Supprime un élément de la todolist */
    .get('/todo/supprimer/:id', function (req, res) {
        if (req.params.id != '') {
            req.session.todolist.splice(req.params.id, 1);
        }
        res.redirect('/todo');
    })

    /*Modifie  une tache  */
    .get('/todo/modif/:id', function (req, res) {
        res.render('modif.ejs', { todolist: req.session.todolist, index: req.params.id })
    })

    // Mise à jour d'une tâche
    .post('/todo/update/:id', urlencodedParser, function (req, res) {
        if (req.params.id != '') {
            req.session.todolist.splice(req.params.id, 1, req.body.todo);
        }
        res.redirect('/todo');
    })

    /* On redirige vers la todolist si la page demandée n'est pas trouvée */
    .use(function (req, res, next) {
        res.redirect('/todo');
    })    
    
    // Gestion des erreurs 404
    .use(function (req, res, next) {
        res.setHeader('Content-Type', 'text/plain');
        res.status(404).send('Page introuvable !');
    })






    

    

    .listen(8080);
